package com.example.bober.clientandroid;

import android.content.Context;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;

/**
 * A client class that allows you to connect to a server in 2 modes.
 * The client launching the test and the client receiving the data.
 */
public class Client {

    //region VARIABLES
    public static final String TAG = Client.class.getCanonicalName();
    private Socket socket;
    private String address;
    private int port;
    private ConnectTest connectTest;
    private Connect connect;
    private OnClientListener listener;
    private int quantityRequest;
    private final int QUANTITY = 30;
    byte[] filedata;

    private final String directoryPath = Environment.getExternalStorageDirectory() + "/clientAndroid";
    private final String filePath = directoryPath + "/test.txt";
    private StringBuilder sb;

    private Context context;
    //endregion

    //region CONSTRUCTOR
    public Client(OnClientListener listener, String address, int port, Context context) {
        this.listener = listener;
        this.address = address;
        this.port = port;
        this.context = context;
    }
    //endregion

    //region CONNECT WITH SERVER
    /**
     * Connection of the test client
     *
     * @param status client mode
     */
    public void connectTest(final int status) {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket(address, port);
                    connectTest = new ConnectTest(socket);
                    if (listener != null) {
                        listener.connectToServer(status);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (listener != null) {
                        listener.noConnectToServer();
                    }
                }
            }
        });
        thread.start();
    }

    /**
     * Connection of the client receiving the data
     *
     * @param status client mode
     */
    public void connect(final int status) {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket(address, port);
                    connect = new Connect(socket);
                    if (listener != null) {
                        listener.connectToServer(status);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (listener != null) {
                        listener.noConnectToServer();
                    }
                }
            }
        });
        thread.start();
    }
    //endregion

    //region FILE METHOD
    /**
     * The contents of the file are returned from the specified file name
     *
     * @param name filename
     * @return file content
     * @throws IOException disconnect
     */
    private byte[] decodeFile(String name) throws IOException {

        InputStream ins = context.getAssets().open(name);
        InputStreamReader ir = new InputStreamReader(ins);
        byte[] fileBytes=new byte[ins.available()];
        ins.read(fileBytes);
        ins.close();

        return fileBytes;
    }

    /**
     * The start of the test is saved
     *
     * @param name filename
     */
    private void openFile(String name) {
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                if (listener != null) {
                    listener.finishTest();
                }
                return;
            }
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));
            DateFormat df = new android.text.format.DateFormat();
            String date = String.valueOf(df.format("yyyy-MM-dd HH:mm:ss", new Date()));
            int size = filedata.length;
            out.append(String.format("%s nazwa pliku: %s rozmiar pliku: %d\n", date, name, size));
            out.close();
        }
        catch (FileNotFoundException e) {
            if (listener != null) {
                listener.finishTest();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * File creation
     */
    private void createFile() {
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Directory creation
     */
    private void createDirectory() {
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }
    //endregion

    //region TEST METHOD

    /**
     * Start test
     *
     * @param name file name
     */
    public void startTest(String name) {
        try {
            filedata = decodeFile(name);
            if (listener != null) {
                listener.showToast("Otworzono plik: " + name + " rozmiar: " + filedata.length);
            }
            quantityRequest = QUANTITY;
            createDirectory();
            createFile();
            openFile(name);
            sb = new StringBuilder();
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    connectTest.sendText();
                }
            });
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region CLIENT FOR TEST

    /**
     * A thread that saves the time the file was sent and received the delivery notification for all messages.
     * After receiving the notification, it sends a test file to the server
     */
    class ConnectTest implements Runnable {
        private DataInputStream inputStream;
        private DataOutputStream outputStream;
        private long startSend;
        private long endSend;

        public ConnectTest(Socket socket){
            try {
                inputStream = new DataInputStream(socket.getInputStream());
                outputStream = new DataOutputStream(socket.getOutputStream());
                Thread thread = new Thread(this);
                thread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            String message="";
            boolean t=true;
            while(t){
                try {
                    message = inputStream.readUTF();
                    endSend = System.currentTimeMillis();
                    if (sb != null) {
                        sb.append(startSend);
                        sb.append("\n");
                        sb.append(endSend);
                        sb.append("\n");
                        sb.append(endSend - startSend);
                        sb.append("\n");
                    }
                    if (listener != null) {
                        listener.showToast("Potwierdzenie odebrania wiadomosci: " + quantityRequest);
                    }
                    quantityRequest--;
                    Log.d(TAG, String.valueOf(startSend));
                    Log.d(TAG, String.valueOf(endSend));
                    if (quantityRequest > 0) {
                        sendText();
                    }
                    else {
                        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));
                        out.append(sb.toString());
                        out.close();
                        if (listener != null) {
                            listener.finishTest();
                        }
                    }
                }
                catch (SocketException | EOFException e) {
                    t = false;
                }
                catch (IOException e) {
                    t = false;
                    e.printStackTrace();
                }
            }

            if (listener != null) {
                listener.finishTest();
            }
        }

        private void sendText() {
            try {
                if (listener != null) {
                    listener.showToast("Wyslano wiadomosc: " + quantityRequest);
                }
                startSend = System.currentTimeMillis();
                outputStream.writeInt(filedata.length);
                outputStream.write(filedata);
            } catch (IOException e) {
                e.printStackTrace();
                if (listener != null) {
                    listener.finishTest();
                }
            }
        }
    }
    //endregion

    //region CLIENT TO RECEIVE DATA

    /**
     * The client thread that is requesting to receive the file and send the message about receiving the message
     */
    class Connect implements Runnable {
        private DataInputStream inputStream;
        private DataOutputStream outputStream;

        public Connect(Socket socket){
            try {
                inputStream = new DataInputStream(socket.getInputStream());
                outputStream = new DataOutputStream(socket.getOutputStream());
                quantityRequest = QUANTITY;
                Thread thread = new Thread(this);
                thread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            String message="";
            int size;
            byte image[];
            boolean t=true;
            while(t){
                try {
                    size = inputStream.readInt();
                    image = new byte[size];
                    inputStream.readFully(image, 0, size);
                    if (listener != null) {
                        listener.showToast("Odebrano zdjecie: " + quantityRequest);
                    }
                    sendText("Request");
                    if (listener != null) {
                        listener.showToast("Wyslano potwierdzenie odebrania zdjecia: " + quantityRequest);
                    }
                    quantityRequest--;
                    if (quantityRequest == 0) {
                        quantityRequest = QUANTITY;
                    }
                }
                catch (SocketException | EOFException e) {
                    t = false;
                }
                catch (IOException e) {
                    t = false;
                    e.printStackTrace();
                }
            }

            if (listener != null) {
                listener.finishTest();
            }
        }

        private void sendText(String message) {
            try {
                outputStream.writeUTF(message);
            } catch (IOException e) {
                e.printStackTrace();
                if (listener != null) {
                    listener.finishTest();
                }
            }
        }
    }
    //endregion

    //region OnClientListener
    /**
     * Interface for communication with activity
     */
    public interface OnClientListener {
        void connectToServer(int status);
        void noConnectToServer();
        void finishTest();
        void showToast(String message);
    }
    //endregion

}
