package com.example.bober.clientandroid;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.IOException;

/**
 * Main activity class
 * It is responsible for creating the view and displaying the messages received by the client's class
 *
 * @author Wojciech Szostak
 * @since 2017.01.03
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements Client.OnClientListener {

    //region VARIABLES
    private AppCompatButton actionButton;
    private AppCompatButton actionButtonTest;
    private CoordinatorLayout coordinatorLayout;
    private TextInputLayout portInputLayout;
    private TextInputLayout addressInputLayout;
    private AppCompatSpinner spinnerFile;
    private AppCompatTextView output;
    private String text;
    private final int CONNECT = 0;
    private final int TEST = 1;
    private int actionStatus = CONNECT;
    private Client client;
    private final int WRITE_EXTERNAL_STORAGE = 0x1;
    private String filenameTest;
    //endregion

    //region LIFE CYCLE
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        grabUI();
        setListener();
        inti();

    }
    //endregion

    //region UI METHOD
    private void grabUI() {
        actionButton = (AppCompatButton) findViewById(R.id.action_button);
        actionButtonTest = (AppCompatButton) findViewById(R.id.action_button_test);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        portInputLayout = (TextInputLayout) findViewById(R.id.input_layout_port);
        addressInputLayout = (TextInputLayout) findViewById(R.id.input_layout_address);
        output = (AppCompatTextView) findViewById(R.id.output_text);
        spinnerFile = (AppCompatSpinner) findViewById(R.id.spinner_file);
    }

    private void inti() {
        text = "";
        Preferences.init(getApplicationContext());
        addressInputLayout.getEditText().setText(Preferences.getIP());
        portInputLayout.getEditText().setText(Preferences.getPort());
        String[] fileList = listAssetFiles("testFile");
        filenameTest = fileList[0];
        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, fileList) {
            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView text = (TextView)view.findViewById(android.R.id.text1);
                text.setTextColor(Color.BLACK);

                return view;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFile.setAdapter(adapter);
    }

    /**
     * Displays snackbar with message text
     *
     * @param message message content
     */
    private void showSnacbar(String message) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * Returns a list of files from the assets directory
     *
     * @param path directory path
     * @return list of files in the directory
     */
    private String[] listAssetFiles(String path) {

        String[] f = new String[0];
        try {
            f = getAssets().list(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }
    //endregion

    //region LISTENERS
    private void setListener() {
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connect(CONNECT);
            }
        });

        actionButtonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (actionStatus) {
                    case CONNECT:
                        connect(TEST);
                        break;
                    case TEST:
                        if (hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            startTest();
                        }
                        else {
                            askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE);
                        }
                        break;
                }
            }
        });

        spinnerFile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                filenameTest = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    //endregion

    //region CLIENT METHOD
    /**
     * Start the test
     */
    private void startTest() {
        actionButtonTest.setEnabled(false);
        client.startTest(filenameTest);
    }

    /**
     * Connection to the server
     *
     * @param status client mode (client with launching test or client receiving data)
     */
    private void connect(int status) {
        try {
            String address = addressInputLayout.getEditText().getText().toString();
            if (isAddresIp(address)) {
                int port = Integer.parseInt(portInputLayout.getEditText().getText().toString());
                client = new Client(MainActivity.this, address, port, getApplicationContext());
                if (status == CONNECT) {
                    client.connect(status);
                }
                else {
                    client.connectTest(status);
                }
            } else {
                showSnacbar("Bad ip address");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            showSnacbar("The wrong data format");
        }
    }

    /**
     * Check if the specified IP address is correct
     * The address must consist of 4 numbers between 0 and 255, separated by a dot
     *
     * @param address IP address
     * @return returns true if the address is correct otherwise false
     */
    private boolean isAddresIp(String address) {
        if (!address.equals("")) {
            String[] split = address.split("\\.");
            if (split.length == 4) {
                try {
                    for (int i = 0; i < 4; i++) {
                        int partAddres = Integer.parseInt(split[i]);
                        if (partAddres <= 0 && partAddres >= 255) {
                            return false;
                        }
                    }
                    return true;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }

        return false;
    }
    //endregion

    //region PERMISSION
    /**
     * This method calls for a permission request
     *
     * @param permission Permission name
     * @param requestCode Cod which is returned answer
     */
    public void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
                //This is called if user has denied the permission before
                // In this case I am just asking the permission again
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            }
        }
    }

    /**
     * The method checks for permission
     *
     * @param permission Permission name
     * @return Returns true or false
     */
    public boolean hasPermission(String permission) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    /**
     * Callback for the result from requesting permissions.
     * If we have permission we initiate test files and display a snackbar
     * If we do not have to let go to close applications
     *
     * @param requestCode The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *     which is either {@link android.content.pm.PackageManager#PERMISSION_GRANTED}
     *     or {@link android.content.pm.PackageManager#PERMISSION_DENIED}. Never null.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case 1:
                    startTest();
                    break;
            }
            showSnacbar("Permission granted");
        } else {
            showSnacbar("Permission denied");
        }
    }
    //endregion

    //region Client.OnClientListener METHOD
    /**
     * Once properly connected to the server, it blocks the appropriate, hides and shows the view elements
     * It writes the IP address and port number
     *
     * @param status client mode
     */
    @Override
    public void connectToServer(final int status) {
        showSnacbar("Connected to Server");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status == TEST) {
                    actionStatus = TEST;
                    actionButtonTest.setText(getString(R.string.run_test));
                    actionButton.setEnabled(false);
                    addressInputLayout.setEnabled(false);
                    portInputLayout.setEnabled(false);
                    spinnerFile.setVisibility(View.VISIBLE);
                    actionButton.setVisibility(View.INVISIBLE);
                } else {
                    actionButton.setEnabled(false);
                    actionButtonTest.setEnabled(false);
                    addressInputLayout.setEnabled(false);
                    portInputLayout.setEnabled(false);
                }
                Preferences.saveIP(addressInputLayout.getEditText().getText().toString());
                Preferences.savePort(portInputLayout.getEditText().getText().toString());
            }
        });
    }

    /**
     * Displays a message about a server connection failure
     */
    @Override
    public void noConnectToServer() {
        showSnacbar("Not connected to Server");
    }

    /**
     * Upon completion of the test, it unlocks the button for re-testing
     */
    @Override
    public void finishTest() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                actionButtonTest.setEnabled(true);
            }
        });

    }

    /**
     * Displays toast with message text
     *
     * @param message message content
     */
    @Override
    public void showToast(final String message) {
        text = (String.format("%s\n%s", text, message));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                output.setText(text);
            }
        });
    }
    //endregion
}
