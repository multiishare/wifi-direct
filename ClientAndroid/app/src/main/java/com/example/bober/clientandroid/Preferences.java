package com.example.bober.clientandroid;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * A class that supports saving and reading settings
 */
public class Preferences {

    //region VARIABLES
    private static SharedPreferences sharedPref;
    private static final String PREFERANCE_FILE_KEY = "IP";
    private static final String SAVE_IP = "save_ip";
    private static final String SAVE_PORT = "save_port";
    //endregion

    //region INIT
    public static void init(Context context) {
        sharedPref = context.getSharedPreferences(PREFERANCE_FILE_KEY, Context.MODE_PRIVATE);
    }
    //endregion

    //region WRITE AND SAVE METHOD
    public static void saveIP(String ip) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SAVE_IP, ip);
        editor.apply();

    }

    public static String getIP() {
        return sharedPref.getString(SAVE_IP, "");
    }

    public static void savePort(String port) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SAVE_PORT, port);
        editor.apply();
    }

    public static String getPort() {
        return sharedPref.getString(SAVE_PORT, "");
    }
    //endregion

}
