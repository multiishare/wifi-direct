package com.example.bober.serverandroid;

import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * The server class responds with the creation of the server and communication with the clients
 */
public class Server {

    //region VARIABLE
    public static final String TAG = Server.class.getCanonicalName();
    private ServerSocket serverSocket;
    private int port;
    private OnServerListener listener;
    private List<ConnectReceiver> connectList;
    private int connectTester = 0;

    private int threadCount;
    private ExecutorService threadPoolExecutor;
    private Scheduler scheduler;
    private PublishSubject<byte[]> subject;
    private Connect testClient;
    private int  receiveClient;
    //endregion

    //region CONSTRUCTOR
    public Server(OnServerListener listener, int port) {
        this.listener = listener;
        this.port = port;
        this.connectList = new ArrayList<>();
        /**
         * Check the flavor version
         * Create a serial or parallel transmission
         */
        if (BuildConfig.FLAVOR.equals("parrallel")) {
            threadCount = Runtime.getRuntime().availableProcessors();
        }
        else {
            threadCount = 1;
        }
        threadPoolExecutor = Executors.newFixedThreadPool(threadCount);
        scheduler = Schedulers.from(threadPoolExecutor);
        receiveClient = 0;
    }
    //endregion

    //region SERVER METHOD

    /**
     * The method that creates the server on the appropriate port
     */
    public void startServer() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(port);
                    if (listener != null) {
                        listener.startServerSuccess();
                        listener.setAddressIp(getIpAddress());
                    }
                    listen();
                } catch (IOException e) {
                    e.printStackTrace();
                    if (listener != null) {
                        listener.startServerFail("Cannot run server");
                    }
                }
            }
        });
        thread.start();
    }

    /**
     * Method of waiting for client connection
     */
    private void listen() {
        if (listener != null) {
            listener.waitOnClients();
        }
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                if (connectTester == 0) {
                    testClient = new Connect(socket);
                }
                else {
                    connectList.add(new ConnectReceiver(socket, connectList.size()));
                }
                connectTester++;
                if (listener != null) {
                    listener.displayText("lista size: " + connectList.size());
                }
                if (listener != null) {
                    listener.connectedToClient();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Return ip address
     *
     * @return IP address
     */
    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> eNI = NetworkInterface.getNetworkInterfaces();
            while (eNI.hasMoreElements()){
                NetworkInterface nI = eNI.nextElement();
                Enumeration<InetAddress> eIA = nI.getInetAddresses();
                while (eIA.hasMoreElements()) {
                    InetAddress iA = eIA.nextElement();
                    if (iA.isSiteLocalAddress()) {
                        ip = String.format("%s%s:%d", ip, iA.getHostAddress(), port);
                    }

                }

            }

        }catch (SocketException e) {
            e.printStackTrace();
            ip += "Error " + e.toString() + "\n";
        }

        return ip;
    }
    //endregion

    /**
     * Mark the delivery confirmation to the customer
     */
    private synchronized void receiveClient() {
        receiveClient++;
        if (receiveClient == connectList.size()) {
            receiveClient = 0;
            testClient.sendText("receive");
            Log.d(TAG, "receiveClient");
            if (listener != null) {
                listener.displayText("Wyslano potwierdzenie o dostarczeniu zdjecie do wszystkich urzadzen");
            }
        }
    }

    /**
     * Interface for communication with activity
     */
    public interface OnServerListener {
        void startServerSuccess();
        void startServerFail(String message);
        void setAddressIp(String addressIp);
        void waitOnClients();
        void connectedToClient();
        void displayText(String message);
    }

    /**
     * The thread responsible for receiving data from the test client and sending the data to the remaining clients
     */
    class Connect implements Runnable {
        private DataInputStream inputStream;
        private DataOutputStream outputStream;

        public Connect(Socket socket){
            try {
                inputStream = new DataInputStream(socket.getInputStream());
                outputStream = new DataOutputStream(socket.getOutputStream());
                subject = PublishSubject.create();
                Thread thread = new Thread(this);
                thread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            String message="";
            int size;
            byte image[];
            boolean t=true;
            while(t){
                try {
                    size = inputStream.readInt();
                    image = new byte[size];
                    inputStream.readFully(image, 0, size);
                    if (listener != null) {
                        listener.displayText("Odebrano zdjecie");
                    }
                    subject.onNext(image);
                }
                catch (SocketException | EOFException e) {
                    t = false;
                }
                catch (IOException e) {
                    t = false;
                    e.printStackTrace();
                }
            }
            connectTester--;
            //connectList.remove(this);
            if (listener != null) {
                listener.displayText("lista size: " + connectList.size());
            }
        }

        private void sendText(String message) {
            try {
                outputStream.writeUTF(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Sending data to other clients
     * Listened to the stream. After receiving the data, send data to the client
     */
    class ConnectReceiver {
        private DataInputStream inputStream;
        private DataOutputStream outputStream;
        private int id;

        public ConnectReceiver(Socket socket, final int id){
            try {
                this.id = id;
                inputStream = new DataInputStream(socket.getInputStream());
                outputStream = new DataOutputStream(socket.getOutputStream());

                subject
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(scheduler)
                        .subscribe(new Observer<byte[]>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(byte[] bytes) {
                                try {
                                    Log.d(TAG, id + " wysylam zdjecie");
                                    outputStream.writeInt(bytes.length);
                                    outputStream.write(bytes);
                                    String message = inputStream.readUTF();
                                    Log.d(TAG, id + " " + message);
                                    receiveClient();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
