package com.example.bober.serverandroid;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;

/**
 * Main activity class
 * It is responsible for creating the view and displaying the messages passed to the server class
 *
 * @author Wojciech Szostak
 * @since 2017.01.03
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements Server.OnServerListener {

    //region VARIABLES
    private Server server;
    private CoordinatorLayout coordinatorLayout;
    private Button startServer;
    private AppCompatTextView addressIpTextView;
    private TextInputLayout portInputLayout;
    private AppCompatTextView output;
    private String text;
    //endregion

    //region LIFE CYCLE
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        grabUI();
        setListeners();
        init();

    }

    private void grabUI() {
        startServer = (Button) findViewById(R.id.start_server_button);
        addressIpTextView = (AppCompatTextView) findViewById(R.id.addres_ip_text_view);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        portInputLayout = (TextInputLayout) findViewById(R.id.input_layout_port);
        output = (AppCompatTextView) findViewById(R.id.output_text);
    }

    private void init() {
        text = "";
    }

    private void setListeners() {
        startServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    server = new Server(MainActivity.this, Integer.parseInt(portInputLayout.getEditText().getText().toString()));
                    server.startServer();
                }
                catch (NumberFormatException e) {
                    e.printStackTrace();
                    showSnacbar("The wrong data format");
                }
            }
        });
    }
    //endregion

    //region METHOD INTERFACE
    @Override
    public void startServerSuccess() {
        showSnacbar("Server running");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startServer.setEnabled(false);
            }
        });
    }

    @Override
    public void startServerFail(String message) {
        showSnacbar(message);
    }

    @Override
    public void setAddressIp(final String addressIp) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                addressIpTextView.setText(addressIp);
                addressIpTextView.setVisibility(View.VISIBLE);
                portInputLayout.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void waitOnClients() {
        showSnacbar("Wait on customers");
    }

    @Override
    public void connectedToClient() {
        showSnacbar("Connected to client");
    }

    /**
     * Display text in a text view
     *
     * @param message message content
     */
    @Override
    public void displayText(final String message) {
        text = (String.format("%s\n%s", text, message));
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                output.setText(text);
            }
        });
    }
    //endregion

    /**
     * Displays snackbar with message text
     *
     * @param message message content
     */
    private void showSnacbar(String message) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

}
